//
//  Coordinator.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 12/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import UIKit

protocol Coordinator {
    var rootController: UINavigationController { get set }
    var childCoordinators: [Coordinator] { get set }

    func start()
}
