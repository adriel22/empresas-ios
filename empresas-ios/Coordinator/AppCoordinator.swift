//
//  AppCordinator.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 12/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import UIKit
final class AppCoordinator: Coordinator {
    var rootController: UINavigationController
    var childCoordinators = [Coordinator]()

    init(navigation: UINavigationController) {
        self.rootController = navigation
    }
    func start() {
        let controller = LoginViewControler()
        controller.coordinator = self
        rootController.pushViewController(controller, animated: false)
    }
    func logIn(information: UserAuthentication) {
        let controller = HomeViewController(authenticationInfo: information)
        controller.coordinator = self
        rootController.pushViewController(controller, animated: true)
    }

}
