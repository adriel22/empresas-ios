//
//  AppDelegate.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 10/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: AppCoordinator?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let navigationController = UINavigationController()
        coordinator = AppCoordinator(navigation: navigationController)
        coordinator?.start()
        window = UIWindow()
        window?.rootViewController = coordinator?.rootController
        window?.makeKeyAndVisible()
        return true
    }

}
