//
//  UserAuthentication.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 12/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import Foundation
struct UserAuthentication {
    var client: String?
    var uid: String?
    var accessToken: String?
}
