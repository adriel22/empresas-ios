//
//  Enterprise.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 13/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import Foundation
struct Enterprise: Codable {
    var description: String
    var enterpriseName: String

    private enum CodingKeys: String, CodingKey {
        case description
        case enterpriseName = "enterprise_name"
    }
}
struct EnterpriseRequest: Codable {
    let enterprises: [Enterprise]
}
