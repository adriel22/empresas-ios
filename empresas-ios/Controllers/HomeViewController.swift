//
//  HomeViewController.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 12/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import UIKit
import Alamofire
final class HomeViewController: UIViewController {
    // MARK: - Properties
    weak var coordinator: AppCoordinator?
    private let customView = HomeView()
    private var searchName = ""
    private let authenticationInfo: UserAuthentication
    var timer: Timer?

    // MARK: - Initializers
    init(authenticationInfo: UserAuthentication) {
        self.authenticationInfo = authenticationInfo
        super.init(nibName: nil, bundle: nil)
        customView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods
    override func loadView() {
        view = customView
    }
    @objc private func searchForEnterprise() {
        print("Searching")
        let parameters: Parameters = ["access-token": authenticationInfo.accessToken ?? "",
                                      "uid": authenticationInfo.uid ?? "",
                                      "client": authenticationInfo.client ?? "", "name": searchName]
        AF.request("https://empresas.ioasys.com.br/api/v1/enterprises", method: .get, parameters: parameters)
            .responseJSON { (response) in
                if let data = response.data {
                    if let dados = try? JSONDecoder().decode(EnterpriseRequest.self, from: data) {
                        self.customView.updateTable(enterprises: dados.enterprises)
                    }
                }

        }
    }
}

extension HomeViewController: HomeViewDelegate {

    func didSearchFor(search: String) {
        timer?.invalidate()
        searchName = search
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchForEnterprise),
                                     userInfo: nil, repeats: false)
    }

}
