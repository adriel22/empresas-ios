//
//  ViewController.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 10/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import UIKit
import Alamofire

final class LoginViewControler: UIViewController {
    // MARK: - Properties
    weak var coordinator: AppCoordinator?
    private let customView = LoginView()
    private let dispatchGroup = DispatchGroup()
    private var state = State.normal {
        didSet {
            updateState()
        }
    }
    private var userAuthetication = UserAuthentication()

    // MARK: - methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        customView.loginButtonAction = buttonAction
    }

    func buttonAction() {
        loginRequest()
        dispatchGroup.enter()
        self.state = .loading
    }
    private func loginRequest() {
        let parameters: Parameters = ["email": customView.email ?? "", "password": customView.password ?? ""]
        AF.request("https://empresas.ioasys.com.br/api/v1/users/auth/sign_in", method: .post,
                   parameters: parameters).responseJSON { (response) in
                    switch response.result {
                    case .failure(let error):
                        print(error)
                    case .success:
                        self.userAuthetication.accessToken = response.response?.allHeaderFields["access-token"]
                            as? String
                        self.userAuthetication.client = response.response?.allHeaderFields["client"] as? String
                        self.userAuthetication.uid = response.response?.allHeaderFields["uid"] as? String
                    }
                    self.dispatchGroup.leave()
        }
    }
    private func updateState() {
        switch state {
        case .loading:
            customView.showLoading()
            dispatchGroup.notify(queue: .main) {
                if self.userAuthetication.accessToken == nil {
                    self.state = .fail
                    return
                }
                self.state = .success
            }
        case .fail:
            customView.hideLoading()
            customView.showWarnings()
        case .success:
            coordinator?.logIn(information: userAuthetication)
        case .normal:
            print("do normal things")
        }
    }

    override func loadView() {
        view = customView
    }

}
enum State {
    case loading
    case success
    case fail
    case normal
}
