//
//  HomeTableDataSource.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 13/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import UIKit
final class HomeTableDataSource: NSObject, UITableViewDataSource {
    var enterprises: [Enterprise]?
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = enterprises?[indexPath.row].enterpriseName
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterprises?.count ?? 0
    }
}
