//
//  HomeViewDelegate.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 13/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import UIKit
protocol HomeViewDelegate: AnyObject {
    func didSearchFor(search: String)
}
