//
//  HomeView.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 12/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import UIKit
final class HomeView: UIView {
    // MARK: - Properties
    private let header: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "homeHeader")!
        return imageView
    }()
    private lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.placeholder = "Pesquise por empresa"
        searchBar.backgroundColor = UIColor(named: "appGrey1")!
        searchBar.searchTextField.backgroundColor = UIColor(named: "appGrey1")!
        searchBar.layer.cornerRadius = 4
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        return searchBar
    }()
    private lazy var table: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .none
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.dataSource = self.tableDataSource
        return table
    }()
    private var headerHeightConstraint = NSLayoutConstraint()
    private let tableDataSource = HomeTableDataSource()
    public weak var delegate: HomeViewDelegate?

    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods
    private func animateUp() {
        headerHeightConstraint.isActive = false
        headerHeightConstraint = header.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.1)
        headerHeightConstraint.isActive = true
    }
    func updateTable(enterprises: [Enterprise]) {
        tableDataSource.enterprises = enterprises
        table.reloadData()
    }
}
extension HomeView: CodableView {
    func buildViewHierarchy() {
        addSubview(header)
        addSubview(searchBar)
        addSubview(table)
    }

    func setupContraints() {
        headerHeightConstraint = header.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3)
        NSLayoutConstraint.activate([
            header.topAnchor.constraint(equalTo: topAnchor),
            header.leadingAnchor.constraint(equalTo: leadingAnchor),
            header.trailingAnchor.constraint(equalTo: trailingAnchor),
            headerHeightConstraint
        ])
        NSLayoutConstraint.activate([
            searchBar.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            searchBar.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            searchBar.heightAnchor.constraint(equalToConstant: 48),
            searchBar.centerYAnchor.constraint(equalTo: header.bottomAnchor)
        ])
        NSLayoutConstraint.activate([
            table.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 50),
            table.bottomAnchor.constraint(equalTo: bottomAnchor),
            table.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            table.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }

    func setupAdditionalConfiguration() {
        backgroundColor = .white
    }

}

extension HomeView: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        animateUp()
        return true
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        delegate?.didSearchFor(search: searchText)
    }

}
