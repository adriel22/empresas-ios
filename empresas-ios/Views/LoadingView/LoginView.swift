//
//  LoginView.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 11/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import UIKit

final class LoginView: UIView {
    // MARK: - Properties
    var email: String?
    var password: String?
    private let header: Header = {
        let view = Header(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let emailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Rubik-Regular", size: 14)!
        label.textColor = UIColor(named: "appGrey3")!
        label.text = "email"
        return label
    }()
    private let passwordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Rubik-Regular", size: 14)!
        label.textColor = UIColor(named: "appGrey3")!
        label.text = "senha"
        return label
    }()
    private lazy var emailTextfield: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = UIColor(named: "appGrey1")!
        textField.layer.cornerRadius = 4
        textField.delegate = self
        return textField
    }()
    private lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = UIColor(named: "appGrey1")!
        textField.layer.cornerRadius = 4
        textField.isSecureTextEntry = true
        textField.delegate = self
        return textField
    }()
    private lazy var loginButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("ENTRAR", for: .normal)
        button.backgroundColor = UIColor(named: "appPink")!
        button.titleLabel?.font = .boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 8
        button.addTarget(self, action: #selector(loginButtonClicked), for: .touchUpInside)
        return button
    }()
    private let warningLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Rubik-Light", size: 12)!
        label.textColor = UIColor(named: "appRed")!
        label.textAlignment = .right
        label.text = "Credenciais incorretas"
        label.isHidden = true
        return label
    }()
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .large
        indicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        indicator.isHidden = true
        return indicator
    }()
    private let opacityView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.layer.opacity = 0.3
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.isHidden = true
        return view
    }()

    private var headerHeightConstraint = NSLayoutConstraint()
    var loginButtonAction: (() -> Void)?

    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - Methods
    private func animateUp() {
        header.animateUp()
        headerHeightConstraint.isActive = false
        headerHeightConstraint = header.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.20)
        headerHeightConstraint.isActive = true
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    private func animateDown() {
        header.animateDown()
        headerHeightConstraint.isActive = false
        headerHeightConstraint = header.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.35)
        headerHeightConstraint.isActive = true
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    @objc private func loginButtonClicked() {
        if let action = loginButtonAction {
            action()
        }
    }

    func showWarnings() {
        warningLabel.isHidden = false
        passwordTextField.rightView = UIImageView(image: UIImage(named: "keyboardErrorIcon"))
        emailTextfield.rightView = UIImageView(image: UIImage(named: "keyboardErrorIcon"))
        passwordTextField.rightViewMode = .always
        emailTextfield.rightViewMode = .always
    }
    func showLoading() {
        opacityView.isHidden = false
        activityIndicator.isHidden = false
        activityIndicator.center = self.center
        activityIndicator.startAnimating()
    }
    func hideLoading() {
        opacityView.isHidden = true
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
}

extension LoginView: CodableView {
    func buildViewHierarchy() {
        addSubview(header)
        addSubview(emailLabel)
        addSubview(emailTextfield)
        addSubview(passwordLabel)
        addSubview(passwordTextField)
        addSubview(loginButton)
        addSubview(warningLabel)
        addSubview(activityIndicator)
        addSubview(opacityView)
    }

    func setupContraints() {
        headerHeightConstraint = header.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.35)
        NSLayoutConstraint.activate([
            header.topAnchor.constraint(equalTo: topAnchor),
            header.leadingAnchor.constraint(equalTo: leadingAnchor),
            header.trailingAnchor.constraint(equalTo: trailingAnchor),
            headerHeightConstraint
        ])
        NSLayoutConstraint.activate([
            emailLabel.topAnchor.constraint(equalTo: header.bottomAnchor, constant: 10),
            emailLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            emailLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            emailLabel.heightAnchor.constraint(equalToConstant: 18)
        ])
        NSLayoutConstraint.activate([
            emailTextfield.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: 4),
            emailTextfield.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            emailTextfield.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            emailTextfield.heightAnchor.constraint(equalToConstant: 48)
        ])
        NSLayoutConstraint.activate([
            passwordLabel.topAnchor.constraint(equalTo: emailTextfield.bottomAnchor, constant: 16),
            passwordLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            passwordLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            passwordLabel.heightAnchor.constraint(equalToConstant: 18)
        ])
        NSLayoutConstraint.activate([
            passwordTextField.topAnchor.constraint(equalTo: passwordLabel.bottomAnchor, constant: 4),
            passwordTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            passwordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            passwordTextField.heightAnchor.constraint(equalToConstant: 48)
        ])
        NSLayoutConstraint.activate([
            loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 40),
            loginButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            loginButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
            loginButton.heightAnchor.constraint(equalToConstant: 48)
        ])
        NSLayoutConstraint.activate([
            warningLabel.heightAnchor.constraint(equalToConstant: 16),
            warningLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            warningLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            warningLabel.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 4)
        ])
    }

    func setupAdditionalConfiguration() {
        backgroundColor = .white
    }

}

extension LoginView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateUp()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateDown()
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == emailTextfield {
            email = textField.text
        } else {
            password = textField.text
        }
    }
}
