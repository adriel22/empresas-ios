//
//  Header.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 11/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import UIKit
final class Header: UIImageView {
    // MARK: - Properties
    private let logo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "logo_home")!
        return imageView
    }()
    private let slogan: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Seja bem vindo ao empresas!"
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont(name: "Rubik-Regular", size: 20)!
        return label
    }()

    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods
    override func layoutSubviews() {
        super.layoutSubviews()

        makeRound(offset: 50)

    }

    private func makeRound(offset: CGFloat) {

        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height - offset))
        path.addLine(to: CGPoint(x: 0, y: self.frame.height - offset))
        path.move(to: CGPoint(x: 0, y: self.frame.height - offset))

        path.addQuadCurve(to: CGPoint(x: self.frame.width, y: self.frame.height - offset),
                          controlPoint: CGPoint(x: self.frame.width / 2, y: self.frame.height ))

        path.close()

        let newLayer = CAShapeLayer()

        newLayer.path = path.cgPath
        self.backgroundColor = .red
        self.layer.insertSublayer(newLayer, at: 0)
        self.layer.mask = newLayer
    }

    func animateUp() {
        self.slogan.layer.opacity = 0
    }
    func animateDown() {
        self.slogan.layer.opacity = 1
    }

}

extension Header: CodableView {
    func buildViewHierarchy() {
        addSubview(logo)
        addSubview(slogan)
    }

    func setupContraints() {
        NSLayoutConstraint.activate([
            logo.widthAnchor.constraint(equalToConstant: 40),
            logo.heightAnchor.constraint(equalToConstant: 32),
            logo.centerXAnchor.constraint(equalTo: centerXAnchor),
            logo.bottomAnchor.constraint(equalTo: centerYAnchor)
        ])
        NSLayoutConstraint.activate([
            slogan.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            slogan.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            slogan.heightAnchor.constraint(equalToConstant: 24),
            slogan.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 16.5)
        ])
    }

    func setupAdditionalConfiguration() {
        image = UIImage(named: "header")
    }

}
