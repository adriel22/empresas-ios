//
//  CodableView.swift
//  empresas-ios
//
//  Created by adriel.de.a.freire on 11/08/20.
//  Copyright © 2020 adriel.de.a.freire. All rights reserved.
//

import Foundation
protocol CodableView {
    func buildViewHierarchy()
    func setupContraints()
    func setupAdditionalConfiguration()
    func setupView()
}

extension CodableView {
    func setupView() {
        buildViewHierarchy()
        setupContraints()
        setupAdditionalConfiguration()
    }
}
